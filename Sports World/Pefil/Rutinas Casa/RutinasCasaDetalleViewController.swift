//
//  RutinasCasaDetalleViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/14/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class RutinasCasaDetalleViewController: UIViewController {
    //MARK: - OUTLETS
    
    @IBOutlet weak var starFive: UIButton!
    @IBOutlet weak var starFour: UIButton!
    @IBOutlet weak var starThree: UIButton!
    @IBOutlet weak var starTwo: UIButton!
    @IBOutlet weak var starOne: UIButton!
    @IBOutlet weak var heartButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    //MARK: - ACCIONES BOTONES

    @IBAction func backButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func clickHeartButton(_ sender: Any) {
    }
    
    @IBAction func clickStarOne(_ sender: Any) {
        starOne.imageView?.image = UIImage(named: "star_gold")
         starTwo.imageView?.image = UIImage(named: "star_white")
         starThree.imageView?.image = UIImage(named: "star_white")
         starFour.imageView?.image = UIImage(named: "star_white")
         starFive.imageView?.image = UIImage(named: "star_white")
    }
    
    @IBAction func clickStarTwo(_ sender: Any) {
        starOne.imageView?.image = UIImage(named: "star_gold")
        starTwo.imageView?.image = UIImage(named: "star_gold")
        starThree.imageView?.image = UIImage(named: "star_white")
        starFour.imageView?.image = UIImage(named: "star_white")
        starFive.imageView?.image = UIImage(named: "star_white")
    }
    @IBAction func clickStarThree(_ sender: Any) {
        starOne.imageView?.image = UIImage(named: "star_gold")
        starTwo.imageView?.image = UIImage(named: "star_gold")
        starThree.imageView?.image = UIImage(named: "star_gold")
        starFour.imageView?.image = UIImage(named: "star_white")
        starFive.imageView?.image = UIImage(named: "star_white")
    }
    
    @IBAction func clickStarFour(_ sender: Any) {
        starOne.imageView?.image = UIImage(named: "star_gold")
        starTwo.imageView?.image = UIImage(named: "star_gold")
        starThree.imageView?.image = UIImage(named: "star_gold")
        starFour.imageView?.image = UIImage(named: "star_gold")
        starFive.imageView?.image = UIImage(named: "star_white")
    }
    @IBAction func clickStarFive(_ sender: Any) {
        starOne.imageView?.image = UIImage(named: "star_gold")
        starTwo.imageView?.image = UIImage(named: "star_gold")
        starThree.imageView?.image = UIImage(named: "star_gold")
        starFour.imageView?.image = UIImage(named: "star_gold")
        starFive.imageView?.image = UIImage(named: "star_gold")
    }
    
    
}
