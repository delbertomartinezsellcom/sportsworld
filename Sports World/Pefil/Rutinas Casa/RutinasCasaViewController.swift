//
//  RutinasCasaViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/14/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class RutinasCasaViewController: UIViewController {
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
  
   
    @IBAction func clickMenuButton(_ sender: Any) {
        DispatchQueue.main.async {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        self.navigationController!.pushViewController(VC1, animated: true)
        }
    }
  
 
    

 

}
