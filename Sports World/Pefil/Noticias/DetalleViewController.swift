//
//  DetalleViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/15/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
class DetalleViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var tituloNoticiaLabel: UILabel!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var subtituloLabel: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
   var tituloFinal = ""
   var subtituloFinal = ""
   var descripcionNoticia = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        tituloNoticiaLabel.text = tituloFinal
        subtituloLabel.text = subtituloFinal
        webView.loadHTMLString(descripcionNoticia, baseURL: nil)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        backView.layer.cornerRadius = 10
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        dismissKeyboard()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
