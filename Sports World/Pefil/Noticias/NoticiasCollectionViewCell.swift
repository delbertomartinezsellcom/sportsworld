//
//  NoticiasCollectionViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 3/15/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class NoticiasCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var descriptionNews: UITextView!
    @IBOutlet weak var titleUnderImage: UILabel!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var imageToShow: UIImageView!
}
