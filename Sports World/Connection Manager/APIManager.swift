
import Foundation
import SwiftyJSON

class APIManager: NSObject {
    
    var urlProd = "http://cloud.sportsworld.com.mx/"
    var allClubesInfo = [[String:AnyObject]]()
    var dictionaryP1 = [[String:AnyObject]]()
    var dictionaryP2 = [[String:AnyObject]]()
    var dictionaryP3 = [[String:AnyObject]]()
    var dictionaryP4 = [[String:AnyObject]]()
    var finalList  = [String]()
    var finalId = [Int]()
    var totalImportes = [String]()
    var totalDescripciones = [String]()
    var listClub = [[String:AnyObject]]()
    var listClubP2 = [[String:AnyObject]]()
    var listClubP3 = [[String:AnyObject]]()
    var listClubP4 = [[String:AnyObject]]()
    var mail: String!
    var message: String!
    var status: Bool!
    var satusString: String!
    var profileImage: String!
    var name: String!
    var codeMessage: Int!
    var userId: Int!
    var gender: String!
    var club: String!
    var memberNumber: Int!
    var height: String!
    var weight: Double!
    var age: Int!
    var member_type: String!
    var mainteiment: String!
    var chargesArray = [[String:AnyObject]]()
    var movimientosArray =  [[String:AnyObject]]()
    var noticiasArray = [[String:AnyObject]]()
    var noticaResumen = [String]()
    var tituloArray = [String]()
    var descripcionArray = [String]()
    var pesoArray = [String]()
    var descripcionArray2 = [String]()
    var imagenNoticia = [String]()
    var nombreConvenio = [String]()
    var clausulasConvenio = [String]()
    var logotipoConvenio = [String]()
    var nombrePase = [String]()
    var finVigencia = [String]()
    var productoPaseInvitado = [String]()
    var cuandoPaseInvitado = [String]()
    static let sharedInstance = APIManager()
    
    func login(authKey: String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params: [String: Any] = [:]
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/login_new/") else {return}
        var  request = URLRequest(url: url)
        
        request.httpMethod = "POST"
        request.addValue(authKey, forHTTPHeaderField: "auth-key")
        print("Este es el authKey", authKey)
        
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    let result =  try JSON(data: data)
                    self.message = json["message"] as? String
                    self.status = json["status"] as! Bool
                    
                    print("Este es el status", self.status)
                    if self.status == false {
                        print("Valio madres")
                    }else {
                        var data = json ["data"] as! Dictionary<String,AnyObject>
                        for element in data {
                            self.profileImage = data["profile_photo"] as? String
                            let name = data["name"] as! String
                            SavedData.setTheName(theName: name ?? "")
                            self.name = name
                            self.userId = data["user_id"] as! Int
                            self.gender = data["gender"] as? String
                            self.club = data ["club"] as? String
                            self.mail = data ["mail"] as? String
                            self.height = data ["tallest"] as? String
                            print("Aqui está el tallest", self.height)
                            self.weight = data ["weight"] as? Double
                            self.age = data["age"] as! Int
                            self.member_type = data ["member_type"] as? String
                            self.mainteiment = data ["mainteiment"] as? String
                            self.memberNumber = data ["membernumber"] as! Int
                            
                            SavedData.setTheUserId(userId: self.userId)
                            
                            
                        }
                        
                    }
                    print(json )
                    
                    
                    onSuccess(result)
                    
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
        
    }
    func register(email: String, tipo: String, club: String, membresia: String, id: String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["email": email,
                                     "tipo" : tipo,
                                     "club" : club,
                                     "membresia" : membresia,
                                     "id" : id]
        guard let url = URL(string: "\(urlProd)app/api/v1/usuario/registro")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result = try JSON(data: data)
                    let mensaje = json["message"]
                    let code = json["code"] as! Int
                    print("Este es el código", code )
                    
                    self.codeMessage = code
                    self.message = mensaje as! String
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
//////////////////////////////////OBTEN LOS MOVIMIENTOS DEL USUARIO/////////////////////////////////////////
    
    func getPendingCharges(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/movimientos/pendientes/\(SavedData.getTheUserId())") else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    print(json)
                    let result = try JSON(data: data)
                    var status = json["status"] as! Bool
                    var message = json["message"] as? String
                    self.message = message
                    self.status = status
                    let dataReceived = json["data"] as! Dictionary<String,AnyObject>
                    
                    for element in dataReceived {
                        let movimientos = dataReceived["movimientos"]
                        self.movimientosArray = movimientos as! [[String : AnyObject]]
                    }
                    for element in self.movimientosArray {
                        let importeTotal = element["importetotal"]
                        let descripcion = element["descripcion"]
                        self.totalImportes.append(importeTotal as! String)
                        self.totalDescripciones.append(descripcion as! String)
                        print("Aqui está el importeTotal", self.totalImportes)
                    }
                    onSuccess(result)
                    print("Todo ok")
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }
        
    
    func getClubesForRegister(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/club_list/\(SavedData.getTheLatitude())/\(SavedData.getTheLongitude())/274978/") else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    print(json)
                    let result = try JSON(data: data)
                    let dataReceived = json["data"]
                    self.allClubesInfo = dataReceived as! [[String : AnyObject]]
                    print("Aqui está la data", self.allClubesInfo)
                    
                    ////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P1///////////////////
                    
                    
                    /*
                     Se repiten los elementos de diccionario p1 con el diccionario p2
                     */
                    /////////////////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P2///////////////////////////////////////////////
                    
                    for allClubesP2 in self.allClubesInfo {
                        let clubReceived = allClubesP2["p2"]
                        self.dictionaryP2 = clubReceived as! [[String : AnyObject]]
                        
                    }
                    
                    for clubList2 in self.dictionaryP1 {
                        let list2 = clubList2["list_club"]
                        self.listClubP2 = list2 as! [[String : AnyObject]]
                    }
                    
                    for clubNameP2 in self.listClubP2 {
                        let nameP2 = clubNameP2["nombre"]
                        //print("Clubes de la p2", nameP2 ?? "")
                        self.finalList.append(nameP2 as! String)
                    }
                    
                    for id2 in self.listClubP2 {
                        let id2 = id2["idun"]
                        //print("id clubes de la p2", id2 ?? 0)
                        self.finalId.append(id2 as! Int)
                    }
                    
                    //////////////////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P3////////////////////////////////
                    
                    for allClubesP3 in self.allClubesInfo {
                        let clubReceived = allClubesP3["p3"]
                        self.dictionaryP3 = clubReceived as! [[String : AnyObject]]
                        
                    }
                    
                    for clubList3 in self.dictionaryP3 {
                        let list3 = clubList3["list_club"]
                        self.listClubP3 = list3 as! [[String : AnyObject]]
                    }
                    
                    for clubNameP3 in self.listClubP3 {
                        let nameP3 = clubNameP3["nombre"]
                        //print("Clubes de la p3", nameP3 ?? "")
                        self.finalList.append(nameP3 as! String)
                    }
                    
                    for id3 in self.listClubP3{
                        let id3 = id3["idun"]
                        //print("id clubes de la p3", id3 ?? 0)
                        self.finalId.append(id3 as! Int)
                    }
                    
                    /////7///////////////////////////////OBTEN LOS CLUBES DEL DICCIONARIO P4///////////////////////////////
                    
                    for allClubesP4 in self.allClubesInfo {
                        let clubReceived = allClubesP4["p4"]
                        self.dictionaryP4 = clubReceived as! [[String : AnyObject]]
                        
                    }
                    
                    for clubList4 in self.dictionaryP4 {
                        let list4 = clubList4["list_club"]
                        self.listClubP4 = list4 as! [[String : AnyObject]]
                    }
                    
                    for clubNameP4 in self.listClubP4 {
                        let nameP4 = clubNameP4["nombre"]
                        self.finalList.append(nameP4 as! String)
                    }
                    
                    
                    for id4 in self.listClubP3{
                        let id4 = id4["idun"]
                        //print("id clubes de la p4", id4 ?? 0)
                        self.finalId.append(id4 as! Int)
                    }
                    
                    print("Lista ids",self.finalId.removeDuplicates())
                    print("Lista clubes",self.finalList.removeDuplicates())
                    onSuccess(result)
                    print("Todo ok")
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }
/////////////////////////////CONEKTA REQUEST///////////////////////////////
    func conekta(token_id: String, person_id: Int, name: String,
                 unit_price:Int, quantity: Int, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["token_id": token_id,
                                     "person_id" : person_id,
                                     "line_items" : ["name" : name,
                                                     "unit_price": unit_price,
                                                     "quantity": quantity]
                                     ]
        guard let url = URL(string: "http://cloud.sportsworld.com.mx/api/v1/app/conekta")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result = try JSON(data: data)
                    let mensaje = json["message"]
                    let code = json["code"]
                    
                    print("Este es el mensaje de conekta", mensaje ?? "")
                    self.message = mensaje as! String
                    self.codeMessage = code as! Int
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
    //////////////////////////////OBTEN PUNTOS EARN IT//////////////////////////////////////////////////
    func earnIt(userId: Int, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["id": userId]
        guard let url = URL(string: "http://manticore.4p.com.mx/manticore/beneficiarioRewards/puntos")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("tokenCliente", forHTTPHeaderField: "5p0r75w0rlD09632")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    let result = try JSON(data: data)
                    let success = json["success"]
                    let mensaje = json["data"]
                    self.status = success as! Bool
                    self.message = mensaje as! String
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
///////////////////////////////////CONFIGURACIÓN////////////////////////////////////////////
   
    func profileUpdate(height: Int, weight: Int, age: Int, img:String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["height": height,
                                     "weight": weight,
                                     "age": age,
                                     "user_id": SavedData.getTheUserId(),
                                     "dob": "1983-05-03",
                                     "memunic_id": 0,
                                     "img": img]
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/profile/update/v2/")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    let result = try JSON(data: data)
                    let success = json["success"]
                    let mensaje = json["data"]
                    self.status = success as! Bool
                    self.message = mensaje as! String
                    
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
///////////////////////////RECUPERAR CONTRASEÑA////////////////////////////
    
    func recoverPassword(email:String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        let params : [String:Any] = ["email": email]
        guard let url = URL(string: "http://cloud.sportsworld.com.mx/app/api/v1/usuario/pass/recuperar")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
      
         request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    let result = try JSON(data: data)
                    let mensaje = json["message"] as? String
                    let code = json ["code"]
                    self.codeMessage = code as! Int
                    self.message = mensaje
                    
                    print("Todo ok")
                    print(json )
                    onSuccess(result)
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
    
//////////////////////////////CONSULTA LAS NOTICIAS///////////////////////
    func getNews(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
        guard let url = URL(string: "https://app.sportsworld.com.mx/api/v2/news") else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    print(json)
                    let result = try JSON(data: data)
                    var status = json["status"] as! Bool
                    self.status = status
                    let dataReceived = json["data"] as! Dictionary<String,AnyObject>
                    
                    for element in dataReceived {
                        let noticias = dataReceived["by_permanent"]
                        self.noticiasArray = noticias as! [[String : AnyObject]]
                    }
                    for element in self.noticiasArray {
                        let resumen = element["resumen"]
                        let titulo = element["titulo"]
                        let descripcion = element["descripcion"]
                        let descripcion2 = element["categorianoticia"]
                        let imagenNoticia = element["rutaimagen"]
                        self.imagenNoticia.append(imagenNoticia as! String)
                        self.descripcionArray2.append(descripcion2 as! String)
                        self.descripcionArray.append(descripcion as! String)
                        self.tituloArray.append(titulo as! String)
                        self.noticaResumen.append(resumen as! String)
                    }
                    onSuccess(result)
                    print("Todo ok")
                    
                    
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }

/////////////////////////////////////////CONSULTA HISTORIAL INBODY//////////////////////////////////////////////////
    func getInbodyHistorial(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/piso/api/v1/inBody/1070137") else {return}
        ////++++++Se hardcodea el idPersona para que regrese la info hay que cambiarlo
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    print(json)
                    let result = try JSON(data: data)
                    var status = json["status"] as! String
                    var  mensaje = json["message"] as! String
                    self.satusString = status
                    self.message = mensaje
                    let dataReceived = json["data"] as!  [[String : AnyObject]]
                    
                    for element in dataReceived {
                      //let peso = dataReceived ["peso"]
                    }
                    onSuccess(result)
                    print("Todo ok")
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }
/////////////////////////////////SEND PASES DE INVITADO///////////////////////////////////
    
        func enviarPasesInvitado(to: String, nameto: String, onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Error) -> Void) {
        
            let params : [String:Any] = ["idPersona": SavedData.getTheUserId(),
                                         "to": to,
                                         "nameto": nameto,
                                         "subject": "Pase de Invitado"]
            
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/app/api/v1/pases/mailPaseInvitado")
            else {return}
        var  request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(SavedData.getSecretKey(), forHTTPHeaderField: "secret-key")
        
        guard  let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return
        }
        request.httpBody = httpBody
        
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    
                    
                    let result = try JSON(data: data)
                    let mensaje = json["message"]
                    let code = json["code"] as! Int
                    self.codeMessage = code
                    self.message = mensaje as! String
                    print("Todo ok")
                    
                    print(json )
                    onSuccess(result)
        
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            
            if(error != nil){
                onFailure(error!)
                print("Todo mal")
            } else{
                
            }
        })
        task.resume()
    }
//////////////////////////// GET CONVENIOS//////////////////////////////////////////
    
    func getConvenios(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Bool) -> Void) {
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/beneficios/api/v1/readBeneficio/1") else {return}
        ////++++++Se hardcodea el idPersona para que regrese la info hay que cambiarlo
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! [[String:Any]]
                    print(json)
                    let result = try JSON(data: data)
                  
                    for element in json {
                        var idBeneficio = element["idBeneficio"] as! Int
                        var nombre = element["nombre"] as! String
                        var clausulas = element["clausulas"] as! String
                        var logotipo = element["logotipo"] as! String
                        self.nombreConvenio.append(nombre)
                        self.clausulasConvenio.append(clausulas)
                        self.logotipoConvenio.append(logotipo)
                    }
                    onSuccess(result)
                    print("Todo ok")
                } catch {
                    print(error)
                    print("Ocurrió un error")
                }
            }
            if(error != nil){
                onFailure(error! as! Bool)
                print("Todo mal")
            } else{
            }
        })
        task.resume()
    }

///////////////////////////////////CONECTA PASES POR MEMBRESIA////////////////////////////////
    func getPasesDeInvitado(onSuccess: @escaping(JSON) -> Void, onFailure: @escaping(Bool) -> Void) {
        guard let url = URL(string: "http://sandbox.sportsworld.com.mx/app/api/v1/pases/readPasesPorMembresia/59843") /*cambiar el id de usuario de SavedData*/else {return}
        ////++++++Se hardcodea el idPersona para que regrese la info hay que cambiarlo
        var  request = URLRequest(url: url)
        request.httpMethod = "GET"
        let session = URLSession.shared
        let task = session.dataTask(with: request as URLRequest, completionHandler: {data, response, error -> Void in
            if let response = response {
                print("Response",response)
            }
            if let data = data {
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! Dictionary<String,AnyObject>
                    print(json)
                    let result = try JSON(data: data)
                    var pases = json["pases"] as! [[String:AnyObject]]
                    var asignados = json["asignados"] as! [[String:AnyObject]]
                    for element in pases {
                        let nombre = element["nombre"] as! String
                        let finVigencia = element["finVigencia"] as! String
                        self.finVigencia.append(finVigencia)
                        self.nombrePase.append(nombre)
                        
                    }
                    for element in asignados {
                        let producto = element["producto"] as! String
                        let cuando = element["cuando"] as! String
                        self.productoPaseInvitado.append(producto)
                        self.cuandoPaseInvitado.append(cuando)
                        
                    }
                    onSuccess(result)
                } catch {
                    print(error)
                }
            }
            if(error != nil){
                onFailure(error! as! Bool)
            } else{
            }
        })
        task.resume()
    }
}





