//
//  InformacionViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 15/01/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class InformacionViewController: UIViewController, ZCarouselDelegate {
    
     var images: ZCarousel?
    func ZCarouselShowingIndex(scrollview: ZCarousel, index: Int) {
        print("AHUEVO")
        
    }
    
    var loadViewController: ((_ index: Int) -> Void)?
    
    @IBAction func loadSegment(_ sender: Any) {
        
        if loadViewController != nil {
            loadViewController!(0)
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        images = ZCarousel(frame: CGRect( x: self.view.frame.size.width/5,
                                          y: 50,
                                          width: (self.view.frame.size.width/5)*3,
                                          height: 150))
        images?.ZCdelegate = self
        images?.addImages(imagesToUSe: ["location", "pin_ubicaciones", "icon_perfil"])
        
        self.view.addSubview(images!)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(InformacionViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
  
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
