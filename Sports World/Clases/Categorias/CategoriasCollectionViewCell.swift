//
//  CategoriasCollectionViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 5/1/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class CategoriasCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var borderedView: UIView!
    @IBOutlet weak var classCategoryName: UILabel!
    
}
