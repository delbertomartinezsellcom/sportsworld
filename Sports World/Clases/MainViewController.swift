//
//  MainViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 2/27/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
class MainViewController: UIViewController, UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource {
    private var cell = "MainCollectionViewCell"
    
    var arrayOfDays = ["1", "2","3","4", "5"]
    
    let items = ["pizza", "deep dish pizza", "calzone"]
    
    let titles = ["Margarita", "BBQ Chicken", "Pepperoni", "sausage", "meat lovers", "veggie lovers", "sausage", "chicken pesto", "prawns", "mushrooms"]
    var currentSelected = Date()
    let formatter = DateFormatter()
    
    @IBOutlet weak var colleView: UICollectionView!
    @IBOutlet weak var tabView: UITableView!
    @IBOutlet weak var categoriasView: UIView!
    @IBOutlet weak var informacionView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    //Termina la configuración del calendario.
    var days = [Int]()
    var daysDateFormat = [Date]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
  
        tabView.delegate = self
        tabView.dataSource = self
        colleView.delegate = self
        colleView.dataSource = self
        
        colleView.isUserInteractionEnabled = true
        formatter.dateFormat = "dd"
        
        let result = formatter.string(from: currentSelected)
        print("date", result)
        
        let cal = Calendar.current
        var date = cal.startOfDay(for: Date())

        
        date = cal.date(byAdding: .day, value: -10, to: date)!
        for i in 1 ... 20 {
            let day = cal.component(.day, from: date)
            days.append(day)
            date = cal.date(byAdding: .day, value: +1, to: date)!
             daysDateFormat.append(date)
        }
        self.currentSelected = self.daysDateFormat[0]
        print("Dias", days)
        print("Dias", days)
       
        
        
        //Customiza la navigationBar para que sea transparente.
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        //Configura las vistas para mostrar las vistas del segmented
   
        informacionView.isHidden = true
        categoriasView.isHidden = true
        
        let titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .normal)
        segmentedControl.setTitleTextAttributes(titleTextAttributes, for: .selected)

        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(MainViewController.dismissKeyboard))
        //view.addGestureRecognizer(tap)
        
    }
    
    deinit {
        print("\(#function)")
    }
    
@objc func dismissKeyboard() {
    view.endEditing(true)
}
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewWillAppear(_ animated: Bool) {
        segmentedControl.selectedSegmentIndex = 1
    }
    //MARK:- CONFIGURA LA VISTA DEL CALENDARIO.
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return days.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
            let cel = collectionView.dequeueReusableCell(withReuseIdentifier: cell, for: indexPath as IndexPath) as! MainCollectionViewCell
            cel.dayNumberLabel.text = String(days[indexPath.row])
        
        if(self.currentSelected.compare(self.daysDateFormat[indexPath.row]) == ComparisonResult.orderedSame ){
            cel.dayNumberLabel.textColor = UIColor.red
        }else{
            cel.dayNumberLabel.textColor = UIColor.white
        }
        return cel
    }

    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.currentSelected = self.daysDateFormat[indexPath.row];
        for i in 0...(self.colleView.visibleCells.count - 1){
            let tempCell : MainCollectionViewCell = self.colleView.visibleCells[i] as! MainCollectionViewCell
            
            
            tempCell.dayNumberLabel.textColor = UIColor.white
            
        }
        let tempCellSelect : MainCollectionViewCell = self.colleView.cellForItem(at: indexPath) as! MainCollectionViewCell
        tempCellSelect.dayNumberLabel.textColor = UIColor.red
        self.colleView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
    }
  
    //MARK:- BOTONES
    
    @IBAction func clickMenuButton(_ sender: Any) {
        DispatchQueue.main.async {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! MenuViewController
        self.navigationController!.pushViewController(VC1, animated: true)
        }
    }
    
    @IBAction func clickMapaButton(_ sender: Any) {
        let VC1 = self.storyboard!.instantiateViewController(withIdentifier: "ClubesViewController") as! ClubesViewController
        self.navigationController!.pushViewController(VC1, animated: true)
    }
    @IBAction func clickSegment(_ sender: UISegmentedControl) {
     
        if sender.selectedSegmentIndex == 0 {
           informacionView.isHidden = false
            categoriasView.isHidden = true
            tabView.isHidden = true
           
        } else if sender.selectedSegmentIndex == 1 {
            informacionView.isHidden = true
            categoriasView.isHidden = true
            tabView.isHidden = false
           
        } else if sender.selectedSegmentIndex == 2 {
            informacionView.isHidden = true
            categoriasView.isHidden = false
            tabView.isHidden = true
           
        }
    }
        //MARK:- TableView

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 30))
        headerView.backgroundColor = UIColor.clear
        let headerLabel = UILabel(frame: CGRect(x: 30, y: 0, width:
            tableView.bounds.size.width, height: tableView.bounds.size.height))
        headerLabel.font = UIFont(name: "LarkeNeueBold-Bold", size: 20)
        headerLabel.textColor = UIColor.white
        headerLabel.text = self.tableView(tableView, titleForHeaderInSection: section)
        headerLabel.sizeToFit()
        headerView.addSubview(headerLabel)
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 111.0
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return titles.count

    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return  titles[section]
    }
    
    
  //  func sectionIndexTitles(for tableView: UITableView) -> [String]? {
    //    return titles
   // }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return items.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClasesTableViewCell", for: indexPath) as! ClasesTableViewCell
            
          
            cell.nameLabel.text! = items[indexPath.row]
            
            return cell
        }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("touch")
    }
        
    
}

