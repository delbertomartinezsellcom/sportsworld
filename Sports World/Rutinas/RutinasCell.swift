//
//  RutinasCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 4/19/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class RutinasCell: UITableViewCell {

    //Conecta los outlets
    @IBOutlet weak var checkButton: UIButton!
    @IBOutlet weak var imageToShow: UIImageView!
    @IBOutlet weak var exerciseName: UILabel!
    @IBOutlet weak var repetitionsLabel: UILabel!
 
    @IBOutlet weak var videoView: UIWebView!
    override func awakeFromNib() {
        super.awakeFromNib()
    //Configura los elementos.
        checkButton.layer.cornerRadius = 15
        checkButton.layer.borderWidth = 1
        checkButton.layer.borderColor = UIColor.white.cgColor
        
        func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
            return videoView
        }
        
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
