//
//  InbodyTableViewCell.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/8/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class InbodyTableViewCell: UITableViewCell {
    @IBOutlet weak var pesoLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
