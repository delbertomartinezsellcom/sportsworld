//
//  DetailConveniosViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/11/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class DetailConveniosViewController: UIViewController, UITextViewDelegate {

    var descripcionClausula = ""
    @IBOutlet weak var txtView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtView.delegate = self 
        txtView.text! = descripcionClausula
    }

 
    func textViewDidBeginEditing(_ textView: UITextView) {
        dismissKeyboard()
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
        
    }
}
