//
//  SegurosViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 6/5/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

class SegurosViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var myTextField: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
         myTextField.delegate = self

    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        dismissKeyboard()
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
        
    }
}
