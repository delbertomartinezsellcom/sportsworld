//
//  CargosPendietesViewController.swift
//  Sports World
//
//  Created by Aldo Gutierrez Montoya on 5/16/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit

var totalAmount: Double!
class CargosPendietesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    private let cell = "CargosPendietesTableViewCell"
    var totalArrayAmount = [Double]()
    var charges = APIManager.sharedInstance.totalImportes.removeDuplicates()
    var descriptions = APIManager.sharedInstance.totalDescripciones.removeDuplicates()
    @IBOutlet weak var tabView: UITableView!
    
    @IBOutlet weak var pagarButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabView.rowHeight = 170
        

 
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    //MARK:- TABLEVIEW
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return charges.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! CargosPendietesTableViewCell
        cel.amountCharge.text = charges[indexPath.row]
        cel.titleCharge.text = descriptions[indexPath.row]
        
       
        
        return cel
        
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cel = tableView.dequeueReusableCell(withIdentifier: cell, for: indexPath as IndexPath) as! CargosPendietesTableViewCell
         cel.checkImage.image = UIImage(named: "icon_seleccionado")
        cel.amountCharge.text! = charges[indexPath.row]
     
        if let amountToInt = NumberFormatter().number(from: cel.amountCharge.text!) {
            var amountInt = amountToInt.doubleValue
            totalArrayAmount.append(amountInt)
            var totalSum = totalArrayAmount.reduce(0, +)
            totalAmount = totalSum
       
        } else {
            print("Hubo un error")
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
       // let selectedItem = items.objectAtIndex(indexPath.row) as String
       // let itemId = selectedItem.componentsSeparatedByString("$%^")
        // remove from self.selectedItems
       // selectedItems[itemId[1]] = nil
    }
    //MARK:- BOTONES
    
    @IBAction func clickPagarButton(_ sender: Any) {
 
    }
}
