//
//  ForgotPasswordTableViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 10/01/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
var emailForRecoverPassword = String()
class ForgotPasswordTableViewController: UITableViewController, UITextFieldDelegate {
    
    //MARK:- VARIABLES Y OUTLETS
  
    @IBOutlet weak var emailTextField: UITextField!
    var originalContainerFrame: CGRect?
    var currentTextField: UITextField?
    override func viewDidLoad() {
        
        
        super.viewDidLoad()

       
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }

    //MARK:- TEXTFIELDS
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextField?.resignFirstResponder()
         emailForRecoverPassword = emailTextField.text!
         print("Este es el valor de email for recovery", emailForRecoverPassword)
        textField.resignFirstResponder()
        currentTextField = nil
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == emailTextField) {
           
            
            currentTextField?.resignFirstResponder()
        }
        return true
    }
    
   

}
