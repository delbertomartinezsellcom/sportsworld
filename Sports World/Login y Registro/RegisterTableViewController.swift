//
//  RegisterTableViewController.swift
//  Sports World
//
//  Created by Delberto Martinez Salazar on 10/01/18.
//  Copyright © 2018 Delberto Martinez Salazar. All rights reserved.
//

import UIKit
import Foundation
//Creamos variables globales para acceder a ellas desde la clase RegisterViewController que es donde se va a hacer la petición.
var idClub = String()
var finalIdClub = String()
var typeInvited = String()
var emailRegister = String()
var memberRegister = String()
class RegisterTableViewController: UITableViewController, UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate{
    
    //MARK: - OUTLETS
    var pickOption = ["socio", "invitado", "empleado"]
    var allClubes = APIManager.sharedInstance.finalList.removeDuplicates()
    var allClubesIds = APIManager.sharedInstance.finalId.removeDuplicates()
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var clubTextField: UITextField!
    @IBOutlet weak var memberTextField: UITextField!
    @IBOutlet weak var invitedType: UITextField!
   
    //MARK: - VARIABLES
    var originalContainerFrame: CGRect?
    var currentTextField: UITextField?
    var picker : UIPickerView!
    var activeTextField = 0
    var activeTF : UITextField!
    var activeValue = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        //CONFIGURA ASPECTO DE LOS TEXTFIELDS.
   
        emailTextField.isHidden = true
        memberTextField.isHidden = true
        clubTextField.isHidden = true

        
      
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegisterTableViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        // get number of elements in each pickerview
        
        switch activeTextField {
        case 1:
            return pickOption.count
        case 2:
            return allClubes.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
     

        currentTextField?.delegate = self
    
        if currentTextField?.tag == 1 {
               return pickOption[row]
        } else if currentTextField?.tag == 2 {
            return allClubes[row]
        } else {
            pickerView.isHidden = true
        
            return ""
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        // set currect active value based on picker view
    
        if currentTextField?.tag == 1 {
            activeValue = pickOption[row]
            typeInvited = pickOption[row]
             print("Este es el pickOption", typeInvited)
        } else if currentTextField?.tag == 2 {
            activeValue = allClubes[row]
            var idFinales = allClubesIds[row]
            finalIdClub = String(idFinales)
          
            
        } else {
            activeValue = ""
        }
        

       
    }
  

    func pickUpValue(textField: UITextField) {
        // create frame and size of picker view
        picker = UIPickerView(frame:CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: self.view.frame.size.width, height: 216)))
        // deletates
        picker.delegate = self
        picker.dataSource = self
     
        picker.backgroundColor = UIColor.white
        textField.inputView = self.picker
        // toolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.barTintColor = UIColor.white
        toolBar.sizeToFit()
        // buttons for toolBar
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(doneClick))
        doneButton.tintColor = UIColor.red
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(cancelClick))
        toolBar.items = [cancelButton, spaceButton, doneButton]
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
    }

    @objc func doneClick() {
        currentTextField?.text = activeValue
        currentTextField?.resignFirstResponder()
    }
    // cancel
    @objc func cancelClick() {
        currentTextField?.resignFirstResponder()
    }

  
    // MARK: - TABLEVIEW
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    //MARK: - TEXFIELD
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
        
        if currentTextField?.tag == 1 {
            activeTextField = 1
              self.pickUpValue(textField: textField)
            
        } else if currentTextField?.tag == 2 {
            activeTextField = 2
            self.pickUpValue(textField: textField)
        } else {
            
        }
    
  
        }
        
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if invitedType.text == "socio" {
            memberTextField.attributedPlaceholder = NSAttributedString(string: "Membresia", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])
            clubTextField.isHidden = false
            emailTextField.isHidden = false
            memberTextField.isHidden = false
            
            emailRegister = emailTextField.text!
            
            if clubTextField.text == nil {
                clubTextField.text = ""
                
            }
            if memberTextField.text == nil {
                memberTextField.text = ""
             
            } else {
                memberRegister = memberTextField.text!
                print("Aqui está el member register", memberRegister)
            }
            
        } else if invitedType.text == "empleado" {
            
            clubTextField.isHidden = true
            emailTextField.isHidden = false
            memberTextField.isHidden = true
            emailRegister = emailTextField.text!
            
            if clubTextField.text == nil {
                clubTextField.text = ""
                idClub = clubTextField.text!
                
            }
            if memberTextField.text == nil {
                memberTextField.text = ""
                memberRegister = memberTextField.text!
            }
            
        }else if invitedType.text == "invitado"{
            memberTextField.attributedPlaceholder = NSAttributedString(string: "id Invitado", attributes: [NSAttributedStringKey.foregroundColor: UIColor.black])

            emailRegister = emailTextField.text!
            
            clubTextField.isHidden = true
            emailTextField.isHidden = false
            memberTextField.isHidden = false
            
            if clubTextField.text == nil {
                clubTextField.text = ""
                idClub = clubTextField.text!
                
            }
            if memberTextField.text == nil {
               
                memberTextField.text = ""
                memberRegister = memberTextField.text!
            }
            
        }
        
        currentTextField?.resignFirstResponder()
        textField.resignFirstResponder()
        currentTextField = nil
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if(textField == invitedType) {
            if textField.text == "socio" {
                print("Es socio")
                clubTextField.isHidden = false
                emailTextField.isHidden = false
                memberTextField.isHidden = false
            } else if textField.text == "invitado" {
                print("Es invitado")
                clubTextField.isHidden = true
                emailTextField.isHidden = false
                memberTextField.isHidden = false
            } else if textField.text == "empleado" {
                print("Es empleado")
                clubTextField.isHidden = true
                emailTextField.isHidden = false
                memberTextField.isHidden = true
                
            }
            emailTextField.becomeFirstResponder()
        }else if(textField == emailTextField) {
            
            emailRegister = emailTextField.text ?? ""
            
            memberTextField.becomeFirstResponder()
        }else if(textField == memberTextField) {
            memberRegister = memberTextField.text ?? ""
            if invitedType.text == "invitado" {
                currentTextField?.resignFirstResponder()
            } else {
                clubTextField.becomeFirstResponder()
            }
        }else if (textField == clubTextField) {
            invitedType.becomeFirstResponder()
            
        }
        invitedType?.resignFirstResponder()
        return true
    }
}

